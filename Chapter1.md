# Operating System

## Phone         

- IOS
- Android
- blackberry
- windows phone
- symbian
- QNX

## Linux

- redhat
- fedora
- debian
- ubuntu

## Unix

- freeBSD
- solaris
- openBSD

## 目标
- 方便
- 有效
- 扩展性

## 操作系统提供服务

- development
    + editor
        * vi
    + compiler
        * gcc
        * gdb
- 程序运行
- I/O输入输出
- 文件访问控制

## 操作系统的发展过程

```mermaid
graph LR
   A[串行处理   ] --> B[简单批处理系统   ];
   B[简单批处理系统   ] --> C[多道批处理系统   ];
   C[多道批处理系统   ] --> D[分时系统   ];
   D[分时系统   ] --> E[实时系统   ]
```

```mermaid
graph LR
   A[Serial Processing   ] --> B[Simple Batch System   ];
   B[Simple Batch System   ] --> C[Multiprogrammed Batch System   ];
   C[Multiprogrammed Batch System   ] --> D[Time-Sharing System   ];
```

- 串行处理 Serial Processing
    + 处理机制
        * 无操作系统 No operating system
        * 通过控制台运行程序 Computers ran from a console
        * 程序通过输入设备载入计算机 Computers ran from a console with display lights, toggle switches, some form of input device, and a printer
        * 按顺序访问：串行 Users have access to the computer in "series"        
- 简单批处理系统 Simple Batch System        
    + 单道程序设计，独占内存
    + Monitor
        * 对一批作业进行自动处理
        * 内存中只能存放一道作业
    + 处理过程
        * read -> input -> output 
    + 监控程序的功能
        * 内存保护 Memory protection
        * 定时器 Timer
        * 特权指令 Privileged instructions
        * 中断 Interrupts
    + 运行模式 Modes of Operation
        * user mode
        * kernel mode
- 多道批处理系统 Multiprogrammed Batch Systems        
    + 多道程序设计，内存有多道程序，交替使用处理器，提高利用率
    + 多道程序设计
        * 内存中同时存放多个作业 Memory is expanded to hold three, four, or more programs and switch among all of them         
        * 多个作业可并发执行
        * 作业调度程序负责作业的调度
    + 硬件支持
        * I/O中断
        * DMA
    + 特征
        * 多道性，调度性，无序性，无交互能力
- 分时系统 Time-Sharing Systems
    + 多道程序 Can be used to handle multiple interactive jobs      
    + 多用户共享处理器 Processor time is shared among multiple users        
    + 多用户通过不同的终端同时访问系统 Multiple users simultaneously access the system through terminals
    + 特征
        * 多路性
        * 独立性
        * 及时性
        * 交互性
- 实时系统
    + 及时响应，在规定时间内开始或完成
    + 特征
        * 可确定性
        * 可响应性
        * 用户控制
        * 可靠性
        * 故障弱化能力

## 操作系统的主要成就 Major Achievements

- 进程 Processes      
    
    多道系统的产生，操作系统用进程管理程序
    + 运行的程序的实例 An instance of a running program 
    + 活动单元 A unit of activity characterized by a single sequential thread of execution, a current state, and an associated set of system resources         
        * 单一顺序线程 a single sequential thread of execution
        * 当前状态 a current state        
        * 相关系统资源 an associated set of system resources        
    + 进程的组成 Components of a Process      
        * 可执行程序 An executable program
        * 程序相关数据 The associated data needed by the program
        * 程序的执行上下文 The execution context of the program        
            - 进程状态
            - 操作系统用来管理和控制进程所需的所有数据 It is the internal data by which the OS is able to supervise and control the process
            - 处理器寄存器的内容 Includes the contents of the various process registers
            - 进程优先级 Includes information such as the priority of the process
            - 是否在等待I/O事件 Includes information such as whether the process is waiting for the completion of a particular I/O event
            - 在内存中的位置
- 内存管理 Memory management        
    
    提高系统利用率
    + 存储管理的任务 Storage management responsibilities                        
        * 进程隔离 Process isolation                
        * 自动分配和管理 Automatic allocation and management        
        * 支持模块化程序设计 Support of modular programming        
        * 保护和访问控制 Protection and access control        
        * 长期储存 Long-term storage        
    + 虚拟存储 Virtual Memory        
        
        提高内存利用率      
        * 逻辑方式访问储存器 A facility that allows programs to address memory from a logical point of view, without regard to the amount of main memory physically available                
        * 多作业存在内存的要求 Conceived to meet the requirement of having multiple user jobs reside in main memory concurrently
        * 换入，换出
        * 分页机制 Paging      
            - 进程由页组成 Allows processes to be comprised of a number of fixed-size blocks, called pages     
            - 虚拟地址由页号组成 Virtual address consist of a page number  
            - 页存在内存任何地址 Each page of a process may be located anywhere in main memory                 
            - 提供虚拟地址和实地址的动态映射 The paging system provides for a dynamic mapping bwtween the virtual address used in the program and a real address in main memory              
        * 检测缺页时，安排载入
- 信息保护和安全 Information protection and security        
    + 可用性 Availability
    + 保密性 Confidentiality      
    + 数据完整性 Data integrity
    + 认证 Authenticity        
- 调度和资源管理 Scheduling and Recource Management              
    + 公平性 Fairness
    + 有差别的响应性 Differential responsiveness
    + 有效性 Efficiency      
        * 最大吞吐量和最小响应时间
        * 容纳多用户
        * 折中处理矛盾需求




## 现代操作系统的特征

- 微内核 microkernel architecture
    + 内核有最基本功能
    + 单体内核：操作系统提供的多数功能能由一个大内核提供，作为一个进程实现
    + 微内核：只给内核分配一些最基本功能，包括
        * 地址空间      
        * 进程间通信(IPC)      
        * 基本调度
    + 微内核优点
        * 简化了实现
        * 提供了灵活性
        * 适合于分布式环境
- 多线程 multiyhreading
    + 把执行一个应用程序的进程划分为可以同时运行的多个线程
    + 线程
        * 可分派的工作单元
        * 包括处理器的上下文环境，自身的数据区域
        * 顺序执行且可以中断
    + 进程        
        * 一个或多个线程和相关系统资源的集合
        * 通过把应用程序分解成多个线程，程序员更容易控制程序的模块化以及相关事件的时间安排        
- 对称多处理 symmetric multiprocessing        
    + 不仅指计算机硬件体系结构，也指采用该体系结构的操作系统行为。
    + 多个进程或线程可以并行运行
    + 多个处理器对用户来讲是透明的
        * 多个处理器共享内存和I/O设施
        * 所有处理器可以执行同一任务
    + 操作系统在不同的处理器上调度不同的进程或线程，并负责他们之间的同步
    + 对称多处理的优点        
        * 性能：多个进程可以在不同的处理器上同时运行
        * 可用性：单个处理器的失效不会导致机器停止
        * 渐增性：系统性能可以随着增加新的处理器而提高
        * 扩展性：生产商根据处理器的数量提供不同价格与性能的产品 
- 分布式操作系统 distributed operating system      
    + 给用户产生错觉
        * 单一的内存空间
        * 单一的外存空间
        * 统一的存取措施
    + 分布式操作系统变得流行，但技术发展水平仍然落后于单处理器操作系统和对称多处理操作系统
- 面向对象的设计 object-oriented design        
    * 用于给小内核增加模块化的扩展
    * 程序员容易定制操作系统，而不会破坏代码完整性
    * 使分布式工具和分布式操作系统开发变得容易
    * Windows是面向对象的设计                

## 容错性

容错性指系统或部件发生软/硬件错误时，能够继续正常运行的能力。

- 提高容错性的技术
    - 进程隔离
        + 进程的内存，文件存取和执行过程相互隔离
    - 并发隔离
        + 采用并发控制保证进程通信或协作时的正确性
    - 虚拟机
        + 提供更高程度的应用隔离和错误隔离
    - 监测点和回滚机制
        + 检测点是先前程序状态的一个副本，回滚则从检测点重新开始运行


## 多处理器和多核操作系统设计考虑因素

不仅要提供多道系统的所有功能，而且必须提供适应多处理器的额外功能。

- 多核系统设计的挑战
    + 如何有效利用多核的计算能力及如何智能且有效的管理芯片上的资源
- 多核系统并行的三个层次
    + 每个核内部的硬件并行，即指令级并行
    + 每个处理器上潜在的多道程序或多线程程序的执行能力
    + 一个应用程序在多核上以多进程或多线程形式执行潜在的并行能力
- 多核系统设计的常用策略
    + 独立运行子任务
    + 支持并行编程，能为并行子任务有效分配资源        
    + 虚拟方式
        * 随着单个芯片上“核”的增加，为一个进程分配一个或多个核，让处理器专门为这个进程服务        
        * 多核操作系统扮演了一个管理程序的角色，负责为应用程序分配“核”的高层决策，但是在所分配资源的有效使用上不再过多关注








## 主流操作系统简介        

- Windows      
    + 执行体
    + 内核有最基本功能
    + 硬件抽象层
    + 设备驱动
    + 窗口和图形系统      
    + 客户-服务器模型
    + 线程和SMP        
    + Windows对象       
- 传统UNIX
- 现代UNIX
- Linux










































































